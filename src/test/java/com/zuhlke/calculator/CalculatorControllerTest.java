package com.zuhlke.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by maku on 20/10/2016.
 */
@RunWith(SpringRunner.class)
@SpringBootTest() //Creates com.zuhlke.calculator.Application context
@AutoConfigureMockMvc // Injects a Mock MVC instance (Fake implementation of an object)
public class CalculatorControllerTest {
    @Autowired //to populate mvc with application and mockmvc context
            MockMvc mvc;

    @Test
    public void getHello() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("Greetings from Spring Boot!")));
    }


    @Test
    public void testQuerySubtraction() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/subtract?a=2&b=1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("1")));
    }

    @Test
    public void testQueryAddition() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/add?a=2&b=1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("3")));
    }

    @Test
    public void testQueryMultiplication() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/multiply?a=2&b=6")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("12")));
    }

    @Test
    public void testQueryDivision() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/divide?a=2&b=1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("2")));
    }
}