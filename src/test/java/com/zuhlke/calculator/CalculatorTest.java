package com.zuhlke.calculator;

import com.zuhlke.calculator.Calculator;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by maku on 19/10/2016.
 */
public class CalculatorTest {
    Calculator calculator = new Calculator();


    @Test
    public void testAddition() throws Exception {
        assertEquals(4, calculator.add(3, 1));
    }

    @Test
    public void testAdditionWithNegativeNumber() throws Exception {
        assertEquals(6, calculator.add(8, -2));
    }

    @Test
    public void testAdditionWithTwoNegNumbers() throws Exception {
        assertEquals(-10, calculator.add(-8, -2));
    }

    @Test
    public void testAdditionWithZero() throws Exception {
        assertEquals(2, calculator.add(2, 0));
    }

    @Test
    public void testSubtraction() throws Exception {
        assertEquals(5, calculator.subtract(10, 5));
    }

    @Test
    public void testSubtractionWithNegativeResult() throws Exception {
        assertEquals(-5, calculator.subtract(5, 10));
    }

    @Test
    public void testSubtractionFromZero() throws Exception {
        assertEquals(-5, calculator.subtract(0, 5));

    }

    @Test
    public void testSubtractionWithNegNumber() throws Exception {
        assertEquals(15, calculator.subtract(10, -5));

    }

    @Test
    public void testSubtractionWithTwoNegNumbers() throws Exception {
        assertEquals(-5, calculator.subtract(-10, -5));
    }

    @Test
    public void testDivision() throws Exception {
        assertEquals(5, calculator.divide(35, 7));

    }

    @Test(expected = ArithmeticException.class)
    public void testDivisionWithZero() throws Exception {
        calculator.divide(35, 0);

    }

    @Test
    public void testDivisionWithNegNumber() throws Exception {
        assertEquals(-5, calculator.divide(35, -7));

    }

    @Test
    public void testDivisionWithTwoNegNumbers() throws Exception {
        assertEquals(5, calculator.divide(-35, -7));

    }

    @Test
    public void testMultiplication() throws Exception {
        assertEquals(35, calculator.multiply(5, 7));

    }


    @Test
    public void testMultiplyWithNegNumber() throws Exception {
        assertEquals(-35, calculator.multiply(5, -7));

    }

    @Test
    public void testMultiplyWithTwoNegNumbers() throws Exception {
        assertEquals(35, calculator.multiply(-5, -7));

    }

}