package com.zuhlke.calculator;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by maku on 20/10/2016.
 */
@RestController
@Api(description = "Carries out simple calculations", produces = "application/json")
public class CalculatorController {

    @RequestMapping(method = RequestMethod.GET, path = "/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @Autowired
    Calculator calculator;

    @ApiOperation(value = "Carry out addition")
    @RequestMapping(method = RequestMethod.GET, path = "/add")
    public String adding(@RequestParam(value = "a") Integer a, @RequestParam(value = "b") Integer b) {
        int result = calculator.add(a, b);
        return Integer.toString(result);
    }

    @ApiOperation(value = "Carry out subtraction")
    @RequestMapping(method = RequestMethod.GET, path = "/subtract")
    public String subtracting(@RequestParam(value = "a") Integer a, @RequestParam(value = "b") Integer b) {
        int result = calculator.subtract(a, b);
        return Integer.toString(result);
    }

    @ApiOperation(value = "Carry out multiplication")
    @RequestMapping(method = RequestMethod.GET, path = "/multiply")
    public String multiplying(@RequestParam(value = "a") Integer a, @RequestParam(value = "b") Integer b) {
        int result = calculator.multiply(a, b);
        return Integer.toString(result);
    }

    @ApiOperation(value = "Carry out division")
    @RequestMapping(method = RequestMethod.GET, path = "/divide")
    public String dividing(@RequestParam(value = "a") Integer a, @RequestParam(value = "b") Integer b) {
        int result = calculator.divide(a, b);
        return Integer.toString(result);
    }

}