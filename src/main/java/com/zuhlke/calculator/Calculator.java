package com.zuhlke.calculator;

import org.springframework.stereotype.Component;

/**
 * Performs arithmetic operations
 */
@Component
public class Calculator {

    /**
     * This method is used to add two integers.
     *
     * @param a This is the first parameter to add method
     * @param b This is the second parameter to add method
     * @return int This returns sum of a and b.
     */
    public int add(int a, int b) {

        return a + b;
    }

    /**
     * This method is used to subtract two integers.
     *
     * @param a This is the first parameter to subtract method
     * @param b This is the second parameter to subtract method
     * @return int This returns difference of a and b.
     */
    public int subtract(int a, int b) {
        return a - b;
    }

    /**
     * Divides two integers.
     *
     * @param a This is the first parameter to divide method
     * @param b This is the second parameter to divide method
     * @return a divided by b.
     * @throws ArithmeticException if b is zero.
     */
    public int divide(int a, int b) {
        return a / b;
    }


    public int multiply(int a, int b) {
        return a * b;
    }
}


